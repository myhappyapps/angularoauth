import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'Spring Boot Tutorial!';
  message01 = 'Api has not been called yet';

  constructor(private httpClient: HttpClient) {}

  button01() {
    this.httpClient.get('/api/hello', { responseType: 'text' }).subscribe({
      next: (response) => (this.message01 = response),
      error: (error) => (this.message01 = 'Error'),
      complete: () => console.info('complete'),
    });
  }

  login() {
    window.location.href = '/oauth2/authorization/spring';
  }

  logout() {
    window.location.href = '/logout';
  }
}
